package src.better.service;

import src.better.domain.*;

import java.util.ArrayList;
import java.util.List;

public class Personnel {

    private List<Employee> employees;
    private static final String PRINT_PATTERN = "%s gagne %.2f euros";

    public Personnel() {
        this.employees = new ArrayList<>();
        this.ajoutDelaListe();
    }

    public void ajouterEmploye(Employee employee) {
        this.employees.add(employee);
    }

    public void afficherSalaires() {
        employees.forEach(employee -> System.out.println(String.format(PRINT_PATTERN, employee.getName(), employee.calculerSalaire())));
        retourtableau();
    }

    public double salaireMoyen() {
        double total = 0;
        retourtableau();
        for (Employee employee: employees) {
            total += employee.calculerSalaire();
        }
        return total / employees.size();
    }

    public Object[][] retourtableau(){
        int NombreEmployee = employees.size();
        Object[][] retour = new Object[NombreEmployee][6];
        int i = 0;
        for (Employee employee: employees) {
            retour[i][0] = employee.getLastname();
            retour[i][1] = employee.getFirstname();
            retour[i][2] = employee.getAge();
            retour[i][3] = employee.getEntryYear();
            retour[i][4] = employee.calculerSalaire();
            retour[i][5] = employee.getPoste();
            i++;
        }
        return retour;
    }


    private void ajoutDelaListe() {
        for (int i=0; i < 30; ++i) {
            this.ajouterEmploye(new src.better.domain.Vendeur("Exemple", "Exemple", 42, "10001", 00));
        }

    }
}
