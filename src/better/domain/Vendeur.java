package src.better.domain;

public class Vendeur extends Seller {

    private String poste = "Vendeur";

    public Vendeur(String firstname, String lastname, int age, String entryYear, double turnover) {
        super(firstname, lastname, age, entryYear, turnover);
    }

    @Override
    public String getPosition() {
        return "Le vendeur";
    }

    @Override
    protected double getBonus() {
        return 400;
    }

    @Override
    public String getPoste() {
        return poste;
    }
}
