package src.better.domain;

public class ManutARisque extends Manutentionnaire implements ARisque {

    private String poste = "ManutARisque";

    public ManutARisque(String firstname, String lastname, int age, String entryYear, int base) {
        super(firstname, lastname, age, entryYear, base);
    }

    @Override
    public double calculerSalaire() {
        return super.calculerSalaire() + getPrime();
    }

    @Override
    public double getPrime() {
        return 200;
    }

    @Override
    public String getPoste() {
        return poste;
    }
}
