
import src.better.domain.*;
import src.better.service.Personnel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controleur implements ActionListener {

    private VuePage vue;
    private Personnel personnel;

    public Controleur(VuePage vue) {
        this.vue = vue;
        this.personnel = new Personnel();
    }


    @Override
    public void actionPerformed(ActionEvent event) {

        JButton source = (JButton) event.getSource();

        if (source.getText().equals("Retour")) {
            vue.changementVueDebut();
        }

        if (source.getText().equals("Valider")) {

            JTextField[] valeur = this.vue.getValeurEntrer();
            if (valeurNonNul(valeur)) {
                if (isInteger(valeur[2].getText())) {
                    if (isInteger(valeur[4].getText()) || isDouble((valeur[4].getText()))) {
                        switch (vue.getPostetList().getSelectedItem().toString()) {
                            case "Manutentionnaire":
                                personnel.ajouterEmploye(new Manutentionnaire(valeur[0].getText(), valeur[1].getText(), Integer.parseInt(valeur[2].getText())
                                        , valeur[3].getText(), Integer.parseInt(valeur[4].getText())));
                                break;
                            case "ManutARisque":
                                personnel.ajouterEmploye(new ManutARisque(valeur[0].getText(), valeur[1].getText(), Integer.parseInt(valeur[2].getText())
                                        , valeur[3].getText(), Integer.parseInt(valeur[4].getText())));
                            case "Representant":
                                personnel.ajouterEmploye(new Representant(valeur[0].getText(), valeur[1].getText(), Integer.parseInt(valeur[2].getText())
                                        , valeur[3].getText(), Double.parseDouble(valeur[4].getText())));
                                break;
                            case "TechnARisque":
                                personnel.ajouterEmploye(new TechnARisque(valeur[0].getText(), valeur[1].getText(), Integer.parseInt(valeur[2].getText())
                                        , valeur[3].getText(), Integer.parseInt(valeur[4].getText())));
                                break;
                            case "Technicien":
                                personnel.ajouterEmploye(new Technicien(valeur[0].getText(), valeur[1].getText(), Integer.parseInt(valeur[2].getText())
                                        , valeur[3].getText(), Integer.parseInt(valeur[4].getText())));
                                break;
                            case "Vendeur":
                                personnel.ajouterEmploye(new Vendeur(valeur[0].getText(), valeur[1].getText(), Integer.parseInt(valeur[2].getText())
                                        , valeur[3].getText(), Double.parseDouble(valeur[4].getText())));
                                break;
                        }
                        vue.changementVueDebut();
                    } else {
                        vue.msgbox("Erreur votre valeur pour le salaire n'est pas un entier ou double");
                    }
                } else {
                    vue.msgbox("Erreur votre valeur pour l'age n'est pas un entier");
                }
            }else {
                vue.msgbox("Erreur un des champs n'es pas rempli");
            }
        }

        if (source.getText().equals("Ajout")) {
            vue.changementVueAjout();
        }
        if (source.getText().equals("Liste")) {
            Object[][] retour = this.personnel.retourtableau();
            vue.changementVueAffichage(retour,personnel.salaireMoyen());
        }

       }

    private boolean valeurNonNul(JTextField[] valeur) {
        for(int i=0;i<valeur.length;i++)
        {
            if(valeur[i].getText().equals(""))
            {
                return false;
            }
        }
        return true;
    }


    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
        } catch(NumberFormatException e) {

            return false;
        }
        return true;
    }
}
