
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class VuePage extends JFrame {

    private JPanel affichage;
    private JPanel bas;
    private Controleur controleur;
    private JTextField[] valeurEntrer = new JTextField[5];

    private  JComboBox postetList;


    public VuePage(){

        this.setTitle("Salarier");
        this.setSize(700, 700);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);

        controleur = new Controleur(this);


        BorderLayout b = new BorderLayout();
        this.setLayout(b);
        b.setVgap(5);

        affichage = new JPanel();
        bas = new JPanel();

        this.add(affichage,  BorderLayout.CENTER);
        this.add(bas,  BorderLayout.CENTER);

        this.changementVueDebut();
    }

    public void changementVueDebut(){

        //Panel
        this.remove(this.affichage);
        this.remove(this.bas);
        affichage = new JPanel();

        this.repaint();
        this.add(affichage,  BorderLayout.CENTER);
        GridLayout gl = new GridLayout(2,1);
        gl.setHgap(10);
        affichage.setLayout(gl);

        //Bouton Ajout
        JButton ajout = new JButton("Ajout");
        ajout.addActionListener(controleur);
        affichage.add(ajout);

        //Bouton Liste
        JButton liste = new JButton("Liste");
        liste.addActionListener(controleur);
        affichage.add(liste);

        this.pack();
        this.repaint();

    }

    public void changementVueAffichage(Object[][] tab,Double salaireMoyen){

        this.remove(this.affichage);
        affichage = new JPanel();
        this.repaint();
        this.add(affichage,  BorderLayout.CENTER);

        String[] entetes = {"Prénom", "Nom", "Age", " Année d'entrer", "Salaire", "Poste"};

        JTable tableau = new JTable(tab, entetes);

        affichage.add(new JScrollPane(tableau));


        bas = new JPanel();
        this.add(bas,  BorderLayout.SOUTH);

        JButton retour = new JButton("Retour");
        retour.addActionListener(controleur);
        bas.add(retour);

        JLabel labelSMoyen = new JLabel("   Salaire Moyen : ");
        bas.add(labelSMoyen);
        JLabel sMoyen = new JLabel(String.valueOf(salaireMoyen));
        bas.add(sMoyen);

        this.add(this.affichage);
        this.pack();
        this.repaint();
    }

    public void changementVueAjout(){

        this.remove(this.affichage);
        affichage = new JPanel();
        this.repaint();
        GridLayout gmillieu = new GridLayout(6, 2); //////
        affichage.setLayout(gmillieu);
        this.add(affichage,  BorderLayout.CENTER);

        valeurEntrer = new JTextField[5];

        //Selection De Poste
        JLabel vide2 = new JLabel("Poste");
        affichage.add(vide2);
        String[] tabPoste = { "Manutentionnaire", "ManutARisque", "Representant", "TechnARisque", "Technicien","Vendeur" };
        postetList = new JComboBox(tabPoste);
        postetList.setSelectedIndex(5);
        affichage.add(postetList);

        //Prenom
        JLabel labelPrenom = new JLabel("Prenom");
        affichage.add(labelPrenom);
        JTextField entrerPrenom = new JTextField("");
        affichage.add(entrerPrenom);
        valeurEntrer[0] = entrerPrenom;

        //NOM
        JLabel labelNom= new JLabel("Nom");
        affichage.add(labelNom);
        JTextField entrerNom = new JTextField("");
        affichage.add(entrerNom);
        valeurEntrer[1] = entrerNom;

        //Age
        JLabel labelAge= new JLabel("Age");
        affichage.add(labelAge);
        JTextField entrerAge = new JTextField("");
        affichage.add(entrerAge);
        valeurEntrer[2] = entrerAge;

        //Anne
        JLabel labelAnne= new JLabel("Année d'entrée");
        affichage.add(labelAnne);
        JTextField EntrerAnne = new JTextField("");
        affichage.add(EntrerAnne);
        valeurEntrer[3] = EntrerAnne;

        //Salaire
        JLabel labelSalaire= new JLabel("Turnover");
        affichage.add(labelSalaire);
        JTextField EntrerSalaire = new JTextField("");
        affichage.add(EntrerSalaire);
        valeurEntrer[4] = EntrerSalaire;


        //Changement d'etat de la liste
        postetList.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e)
            { switch (postetList.getSelectedItem().toString()){
                case "Manutentionnaire": case "ManutARisque":
                    labelSalaire.setText("Heures");
                    break;
                case"Representant": case "Vendeur":
                    labelSalaire.setText("Turnover");
                    break;
                case "TechnARisque": case "Technicien":
                    labelSalaire.setText("Units");
                    break;

                }
            }
        });

        bas = new JPanel();
        this.add(bas,  BorderLayout.SOUTH);

        JButton retour = new JButton("Retour");
        retour.addActionListener(controleur);
        bas.add(retour);

        JButton valider = new JButton("Valider");
        valider.addActionListener(controleur);
        bas.add(valider);

        this.add(this.affichage);
        this.pack();
        this.repaint();
    }


    public JTextField[] getValeurEntrer() {
        return valeurEntrer;
    }

    public JComboBox getPostetList() { return postetList; }

    public void msgbox(String s){
        JOptionPane.showMessageDialog(null, s);
    }



}
